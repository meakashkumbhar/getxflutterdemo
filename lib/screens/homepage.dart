import 'package:demogetx/controllers/homepageController.dart';
import 'package:demogetx/controllers/post_controller.dart';
import 'package:demogetx/screens/addNewPost.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PostController postController = Get.put(PostController());

  @override
  Widget build(BuildContext context) {
    return GetX<PostController>(
      init: PostController(),
      builder: (controller) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Getx Demo App'),
          ),
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(5.0),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: controller.loading.value
                  ? const Center(child: CircularProgressIndicator())
                  : ListView(
                      children: controller.posts
                          .map<Widget>((element) => ListTile(
                                leading: Text(element.id.toString()),
                                title: Text(element.title),
                                subtitle: Text(element.body),
                                trailing: Text('User ${element.userId}'),
                              ))
                          .toList(),
                    ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const AddNewPost()));
            },
            child: const Icon(Icons.play_circle_outline_sharp),
          ),
        );
      },
    );
  }
}
