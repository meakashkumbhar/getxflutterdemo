import 'package:demogetx/controllers/post_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddNewPost extends StatefulWidget {
  const AddNewPost({super.key});

  @override
  State<AddNewPost> createState() => _AddNewPostState();
}

class _AddNewPostState extends State<AddNewPost> {
  PostController postController = Get.put(PostController());

  final TextEditingController titleTextController = TextEditingController();
  final TextEditingController bodyTextController = TextEditingController();
  final TextEditingController idTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Add New Post')),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Center(
                  child: Text(
                "Load Your Data Here!",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              )),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Add Your title!",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              TextField(
                controller: titleTextController,
                textAlign: TextAlign.left,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter Your Title',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: bodyTextController,
                textAlign: TextAlign.left,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter Your Title',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: idTextController,
                textAlign: TextAlign.left,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter Your Title',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                margin: const EdgeInsets.all(10),
                height: 50.0,
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: const BorderSide(
                          color: Color.fromRGBO(0, 160, 227, 1))),
                  onPressed: () async {
                    postController.bodyText.value = bodyTextController.text;
                    postController.titleText.value = titleTextController.text;
                    postController.idText.value = idTextController.text;
                    String result = await postController.sendPost();
                    if (result == 'Sent Data Successfully') {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text("Successfully sent Data")));
                    }
                  },
                  padding: const EdgeInsets.all(10.0),
                  color: const Color.fromRGBO(0, 160, 227, 1),
                  textColor: Colors.white,
                  child:
                      const Text("Submit Form", style: TextStyle(fontSize: 15)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    ;
  }
}
