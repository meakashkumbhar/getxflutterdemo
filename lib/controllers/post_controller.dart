import 'dart:math';

import 'package:demogetx/api/post_provider.dart';
import 'package:demogetx/models/post_model.dart';
import 'package:get/get.dart';

class PostController extends GetxController {
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    getPosts();
  }

  var posts = <Post>[].obs;
  var loading = false.obs;
  var titleText = "".obs;
  var bodyText = "".obs;
  var idText = "".obs;
  PostProvider postProvider = PostProvider();

  getPosts() async {
    loading(true);
    var response = await postProvider.getPosts();
    if (!response.status.hasError) {
      posts.value = postFromJson(response.bodyString);
    }
    loading(false);
  }

  Future<String> sendPost() async {
    loading(true);
    print(bodyText);
    print(titleText);
    print(idText);

    var response = await postProvider.sendPostData(
        {'title': titleText.value, 'body': bodyText.value, 'id': idText.value});
    if (!response.hasError) {
      print("Sent Data Successfully");
      loading(false);
      return 'Sent Data Successfully';
    } else {
      loading(false);
      return 'Something went Wrong!';
    }
  }
}
