import 'package:get/get.dart';

class HomePageController extends GetxController {
  final count = 1.obs;
  final textToShow = "".obs;

  increment() {
    count.value++;
    textToShow.value = "The Positive Button was clicked";
  }

  decrement() {
    count.value--;
    textToShow.value = "The Negative Button was clicked";
  }
}
