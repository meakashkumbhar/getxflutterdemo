import 'package:get/get.dart';

class PostProvider extends GetConnect {
  Future<Response> getPosts() =>
      get('https://jsonplaceholder.typicode.com/posts');

  Future<Response> sendPostData(Map data) =>
      post('https://jsonplaceholder.typicode.com/posts', data,
          contentType: 'application/json; charset=UTF-8');
}
